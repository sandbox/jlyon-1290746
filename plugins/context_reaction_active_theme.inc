<?php

/**
 * Expose themes as context reactions.
 */
class context_reaction_active_theme extends context_reaction {

  /**
   * Allow admins to provide a section title, section subtitle and section class.
   */
  function options_form($context) {
    $options = array();
    $themes = list_themes();
    foreach($themes as $name => $theme) {
      if($theme->status == 1) $options[$name] = $name;
    }

    $settings = $this->fetch_from_context($context);

    $form = array(
      '#tree' => TRUE,
      '#title' => t('Theme'),
      'theme' => array(
        '#title' => t('Active theme'),
        '#description' => t('Choose a theme to activate when this context is active.'),
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => isset($settings['theme']) ? $settings['theme'] : '',
      ),
    );
    return $form;
  }

  /**
   * Return the active theme
   */
  function execute() {
    $theme = NULL;

    foreach ($this->get_contexts() as $k => $v) {
      if ($v->reactions['active_theme']['theme']){
        $theme = $v->reactions['active_theme']['theme'];
      }
    }

    return $theme;
  }
}
